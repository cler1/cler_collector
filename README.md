This repository has been deprecated.

There are now better ways to separately collect behavioral and payment data.

Please go to this page: <https://gitlab.com/cler1/AnonPay>
